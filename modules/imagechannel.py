#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from html.parser import HTMLParser
import functools
import os
import asyncio

from aschedule import every
import aiohttp
import aioredis
import feedparser

rss_hosts = [
    ('http://c-cassandra.tumblr.com/rss', -1001089728920),
    ('http://lizclimo.tumblr.com/rss', -1001079259850),
    ('http://eatmorebikes.tumblr.com/rss', -1001070028055),
    ('http://timetrabble.tumblr.com/rss', -1001078213778),
    ('http://www.webtoons.com/en/challenge/jaywalker-pictures/rss?title_no=34044', -1001059424680),
    ('http://www.webtoons.com/en/challenge/brown-paperbag/rss?title_no=32251', -1001089364860),
    ('http://mercworks.net/feed/', -1001084649703),
    ('http://pickledcomics.tumblr.com/rss', -1001086249750),
    # ('http://tumblr.safelyendangered.com/rss', ''),
    # ('http://pdlcomics.tumblr.com/rss', ''),
    # ('http://zenpencils.tumblr.com/rss', ''),
    # ('http://forlackofabettercomic.tumblr.com/rss', ''),
    # ('http://twitterthecomic.tumblr.com/rss', ''),
    # ('http://threewordphrase.tumblr.com/rss', ''),
    # ('http://upandoutcomic.tumblr.com/rss', '')
]

REDIS_URI = urlparse(os.environ['REDIS_URL'])

REDIS_PASSWORD = REDIS_URI.password
REDIS_HOSTNAME = REDIS_URI.hostname
REDIS_PORT = REDIS_URI.port

BOT_URL = os.environ["TELEGRAM_BOT_URL"]


class ExtractImages(HTMLParser):
    def error(self, message):
        pass

    def __init__(self):
        super(ExtractImages, self).__init__()
        self.images = []

    def handle_starttag(self, tag, attrs):
        if tag == 'img' and 'src' in dict(attrs):
            self.images.append(dict(attrs)['src'])

    def clear(self):
        self.images = []


def extract_images(html):
    ei = ExtractImages()
    ei.feed(html)
    return ei.images

async def scrap_rss(url, channel):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text = await response.text()
        feed = feedparser.parse(text)
        redis = await aioredis.create_redis(
            (REDIS_HOSTNAME, REDIS_PORT),
            password=REDIS_PASSWORD)
        for entry in feed['entries']:
            if not await redis.sismember(url, entry['link']):
                await redis.sadd(url, entry['link'])
                for img_link in extract_images(entry['description']):
                    data = {
                        'chat_id': channel,
                        'text': img_link,
                    }
                    async with session.get(BOT_URL + 'sendMessage', params=data) as response:
                        print(await response.text())  # logging
        redis.close()


def main():
    for url, channel in rss_hosts:
        every(functools.partial(scrap_rss, url, channel), hours=1)

if __name__ == '__main__':
    main()
    asyncio.get_event_loop().run_forever()
