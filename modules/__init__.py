#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

from .imagechannel import main as imagechannel_main
from .linkchannel import main as linkchannel_main


def initialize():
    imagechannel_main()
    linkchannel_main()
