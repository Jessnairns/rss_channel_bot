#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from datetime import datetime, timedelta
import functools
import os
import asyncio

from aschedule import every
import aiohttp
import aioredis
import feedparser

rss_hosts = [
    ('https://eev.ee/feeds/blog.atom.xml', -1001055245174),
    ('http://hack.limbicmedia.ca/rss/', -1001092590412),
    ('http://www.blog.pythonlibrary.org/feed/', -1001098076228),
    ('https://realpython.com/atom.xml', -1001089149252),
    ('https://hackercollider.com/feed.xml', -1001095237702),
    # ('https://kozikow.com/feed/', ),  # machine learning
    ('http://www.giantflyingsaucer.com/blog/?feed=rss2', -1001081877418),
    ('https://dbader.org/rss', -1001098300684),
    ('https://medium.com/feed/@hwayne/', -1001093877096),
    (os.environ["MY_GITHUB_HOME_FEED"], -1001053300559),  # my github home feed private url
    ('http://stackabuse.com/rss/', -1001067394199),
    ('https://hackernoon.com/feed', -1001071577617),
    ('https://blog.anudeep2011.com/feed/', -1001094867532),
    ('https://culture.zomato.com/feed', -1001081440298),
    ('https://engineering.zomato.com/feed', -1001051204488),
]

REDIS_URI = urlparse(os.environ['REDIS_URL'])

REDIS_PASSWORD = REDIS_URI.password
REDIS_HOSTNAME = REDIS_URI.hostname
REDIS_PORT = REDIS_URI.port

BOT_URL = os.environ["TELEGRAM_BOT_URL"]

async def scrap_rss(url, channel):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text = await response.text()
        feed = feedparser.parse(text)
        redis = await aioredis.create_redis(
            (REDIS_HOSTNAME, REDIS_PORT),
            password=REDIS_PASSWORD)
        for entry in feed['entries']:
            if not await redis.sismember(url, entry['link']):
                await redis.sadd(url, entry['link'])
                data = {
                    'chat_id': channel,
                    'text': '[{title}]({link})'.format(title=entry['title'], link=entry['link']),
                    'parse_mode': 'Markdown',
                }
                async with session.get(BOT_URL + 'sendMessage', params=data) as response:
                    print(await response.text())  # logging
        redis.close()


def main():
    for url, channel in rss_hosts:
        # every(functools.partial(scrap_rss, url, channel), hours=1)
        every(functools.partial(scrap_rss, url, channel), hours=1, start_at=datetime.now() + timedelta(minutes=5))

if __name__ == '__main__':
    main()
    asyncio.get_event_loop().run_forever()
